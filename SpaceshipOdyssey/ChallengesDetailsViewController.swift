//
//  ChallengesDetailsViewController.swift
//  SpaceshipOdyssey
//
//  Created by Najibullah Ulul Albab on 12/04/20.
//  Copyright © 2020 Najibullah Ulul Albab. All rights reserved.
//

import UIKit

class ChallengesDetailsViewController: UIViewController {

    @IBOutlet weak var challengeImageView: UIImageView!
    @IBOutlet weak var challengeTitleLabel: UILabel!
    @IBOutlet weak var challengeDescriptionLabel: UILabel!
    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var durationLabel: UILabel!
    @IBOutlet weak var goalsLabel: UILabel!
    @IBOutlet weak var constraintsLabel: UILabel!
    @IBOutlet weak var deliverablesLabel: UILabel!
    
    var imageView: String!
    var titleLabel: String!
    var descriptionLabel: String!
    var challenges: Challenge?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        challengeImageView.image = UIImage(named: challenges!.image)
        challengeTitleLabel.text = challenges?.title
        challengeDescriptionLabel.text = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and"
        typeLabel.text = challenges?.type
        durationLabel.text = "\(challenges!.duration)WEEKS"
        goalsLabel.text = challenges?.goals.joined(separator: ", ")
        constraintsLabel.text = challenges?.constraints.joined(separator: ", ")
        deliverablesLabel.text = challenges?.deliverables.joined(separator: ", ")
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
