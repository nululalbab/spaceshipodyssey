//
//  ChallengesCollectionViewCell.swift
//  SpaceshipOdyssey
//
//  Created by Najibullah Ulul Albab on 12/04/20.
//  Copyright © 2020 Najibullah Ulul Albab. All rights reserved.
//

import UIKit

class ChallengesCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var challengeImageView: UIImageView!
    
 
    @IBOutlet weak var subtitleLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
        // Initialization code
    }
    
    class var reuseIdentifier: String {
        return "ChallengesCollectionViewCellReuseIdentifier"
    }
    
    func configureCell(title: String, subtitle: String, image: String) {
        self.titleLabel.text = title
        self.subtitleLabel.text = subtitle
        self.challengeImageView.image = UIImage(named: image)
    }
    
    
    

}
