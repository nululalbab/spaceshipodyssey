//
//  LearningBacklogCollectionViewCell.swift
//  SpaceshipOdyssey
//
//  Created by Najibullah Ulul Albab on 14/04/20.
//  Copyright © 2020 Najibullah Ulul Albab. All rights reserved.
//

import UIKit

class LearningBacklogCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    let formatter = DateFormatter()
    
    // initially set the format based on your datepicker date / server String
   
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    class var reuseIdentifier: String {
        return "LearningBacklogCollectionViewCellReuseIdentifier"
        
    }
    
    func configureCell(title: String, description: String, date: String) {
        self.titleLabel.text = title
        self.dateLabel.text = date
        self.descriptionLabel.text = description
    }

}
