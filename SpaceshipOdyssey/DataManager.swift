//
//  DataManager.swift
//  SpaceshipOdyssey
//
//  Created by Najibullah Ulul Albab on 14/04/20.
//  Copyright © 2020 Najibullah Ulul Albab. All rights reserved.
//

import Foundation

class DataManager{
    var arrayOfChallenges: [Challenge]
    var arrayOfLearningBacklog: [LearningBacklog]
    var dateComponents = DateComponents()
    var arrayOfExplorers: [Explorers]
    var arrayOfActivities: [Activities]
    var arrayOfResources: [Resources]

    // Create date from components
    
    init(){
        arrayOfChallenges = [
            Challenge(title: "Challenge 1", description: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.", duration: 5,goals: ["Working Prototype", "No Internet Connection", "No AR/Game"], image: "challenge1", team: "Team", type: "Mini",constraints: ["No Game","No Back Ends","No Augmented Reality"], deliverables: ["Working Prototype"]),
            Challenge(title: "Challenge 2", description: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.", duration: 2,goals: ["Working Prototype", "No Internet Connection", "No AR/Game"], image: "challenge2", team: "Individual", type: "Nano",constraints: ["This is individual work","No Third-Party Library","Use HIG interface essentials"], deliverables: ["Sketch Prototype", "Learning Backlog Document", "Xcode Project", "Feedback Report Document"]),
            Challenge(title: "Challenge 3", description: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.", duration: 4,goals: ["User Narration", "Trello for project", "Working app with clean code"], image: "challenge3", team: "Team", type: "Mini",constraints: ["No Constraint"], deliverables: ["User Narration", "Trello for project", "Working app with clean code"]),
            Challenge(title: "Challenge 4", description: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.", duration: 4,goals: ["TBA"], image: "challenge4", team: "Team",type: "TBA",constraints: ["TBA"], deliverables: ["TBA"]),
            Challenge(title: "Challenge 5", description: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.", duration: 4,goals: ["Working Prototype", "No Internet Connection", "No AR/Game"], image: "challenge5", team: "Team",type: "TBA",constraints: ["TBA"], deliverables: ["TBA"]),
            Challenge(title: "Challenge 6", description: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.", duration: 4,goals: ["Working Prototype", "No Internet Connection", "No AR/Game"], image: "challenge6", team: "Team",type: "TBA",constraints: ["TBA"], deliverables: ["TBA"])
        ]
        arrayOfLearningBacklog = [
            LearningBacklog(title: "Implement Core Data", description: "Implementing core data for Swift 5", date: "14/02/2020", resources: ["link","URL"]),
            LearningBacklog(title: "Implement Core Data", description: "Implementing core data for Swift 5", date: "14/02/2020", resources: ["link","URL"])
            ,LearningBacklog(title: "Implement Core Data", description: "Implementing core data for Swift 5", date: "14/02/2020", resources: ["link","URL"])
        ]
        arrayOfExplorers = [
            Explorers(name: "Ahmad Rizki Maulana", status: .junior, shift: "Morning", role: "Tech", photoAssetName: "Rizki_MO", iWantToLearn: "", iCanHelpWith: "Coding Basic"),
            Explorers(name: "Albert Pangestu", status: .junior, shift: "Morning", role: "Tech", photoAssetName: "Albert_MO", iWantToLearn: "Ideation", iCanHelpWith: "Programming Basics"),
            Explorers(name: "Alifudin Aziz", status: .junior, shift: "Morning", role: "Tech", photoAssetName: "Aziz_MO", iWantToLearn: "Ideation", iCanHelpWith: "Programming Basics"),
            Explorers(name: "Gabriele Wijasa", status: .senior, shift: "All", role: "Design Facilitator", photoAssetName: "Gabriele_SE", iWantToLearn: "", iCanHelpWith: "Graphic Design, UI/UX, Photography"),
            Explorers(name: "Januar Tanzil", status: .senior, shift: "All", role: "Coding Facilitator", photoAssetName: "Januar_SE", iWantToLearn: " Graphic Design, UI/UX, Photography", iCanHelpWith: "Swift language, IOS Development, Game Development, Software Engineering"),
            Explorers(name: "Jaya Pranata", status: .senior, shift: "All", role: "Coding Facilitator", photoAssetName: "Jaya_SE", iWantToLearn: " Graphic Design (layout/composition, typography, branding), UI/UX, problem analysis, basic research, HIG", iCanHelpWith: "Coding basic, Swift Language, Auto Layout, Core Data, Map")
        ]
        arrayOfActivities = [
            Activities(name: "What is MLKit", photoAssetName: "ML-Kit", description: "Introduction to ML kit"),
             Activities(name: "Why we use MLKit", photoAssetName: "ML-Kit2", description: "Simple operation of ML kit"),
              Activities(name: "Creating our first MLKit", photoAssetName: "ML-Kit3", description: "Create project with ML kit"),
               Activities(name: "Creating our first MLKit", photoAssetName: "ML-Kit3", description: "Create project with ML kit"),
                Activities(name: "Creating our first MLKit", photoAssetName: "ML-Kit3", description: "Create project with ML kit"),
                 Activities(name: "Creating our first MLKit", photoAssetName: "ML-Kit3", description: "Create project with ML kit")
        ]
        
        arrayOfResources = [
            Resources(name: "What is MLKit", photoAssetName: "ML-Kit", description: "Introduction to ML kit"),
            Resources(name: "Why we use MLKit", photoAssetName: "ML-Kit2", description: "Simple operation of ML kit"),
             Resources(name: "Creating our first MLKit", photoAssetName: "ML-Kit3", description: "Create project with ML kit"),
             Resources(name: "Creating our first MLKit", photoAssetName: "ML-Kit3", description: "Create project with ML kit"),
             Resources(name: "Creating our first MLKit", photoAssetName: "ML-Kit3", description: "Create project with ML kit"),
             Resources(name: "Creating our first MLKit", photoAssetName: "ML-Kit3", description: "Create project with ML kit")
        ]
    }
}
