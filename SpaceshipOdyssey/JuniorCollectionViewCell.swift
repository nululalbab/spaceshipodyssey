//
//  JuniorCollectionViewCell.swift
//  SpaceshipOdyssey
//
//  Created by Najibullah Ulul Albab on 16/04/20.
//  Copyright © 2020 Najibullah Ulul Albab. All rights reserved.
//

import UIKit

class JuniorCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var roleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    func configureCell(name: String, role: String, image: String) {
        self.nameLabel.text = name
        self.roleLabel.text = role
        self.image.image = UIImage(named: image)
    }

}
