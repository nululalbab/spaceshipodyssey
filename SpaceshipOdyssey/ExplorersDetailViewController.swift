//
//  ExplorersDetailViewController.swift
//  SpaceshipOdyssey
//
//  Created by Najibullah Ulul Albab on 16/04/20.
//  Copyright © 2020 Najibullah Ulul Albab. All rights reserved.
//

import UIKit

class ExplorersDetailViewController: UIViewController {
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var roleLabel: UILabel!
    @IBOutlet weak var helpLabel: UILabel!
    @IBOutlet weak var cardView: UIView!
    
    @IBOutlet weak var needHelpLabel: UILabel!
   var explorers: Explorers?
    override func viewDidLoad() {
        super.viewDidLoad()
        nameLabel.text = explorers?.name
        image.image = UIImage(named: explorers!.photoAssetName)
        roleLabel.text = explorers?.role
        helpLabel.text = explorers?.iCanHelpWith
        needHelpLabel.text = explorers?.iWantToLearn
        cardView.layer.cornerRadius = 20
        cardView.layer.masksToBounds = true
//        challengeTitleLabel.text = challenges?.title
//        challengeDescriptionLabel.text = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and"
//        typeLabel.text = challenges?.type
//        durationLabel.text = "\(challenges!.duration)WEEKS"
//        goalsLabel.text = challenges?.goals.joined(separator: ", ")
//        constraintsLabel.text = challenges?.constraints.joined(separator: ", ")
//        deliverablesLabel.text = challenges?.deliverables.joined(separator: ", ")
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
