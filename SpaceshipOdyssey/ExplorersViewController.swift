//
//  ExplorersViewController.swift
//  SpaceshipOdyssey
//
//  Created by Najibullah Ulul Albab on 15/04/20.
//  Copyright © 2020 Najibullah Ulul Albab. All rights reserved.
//

import UIKit

class ExplorersViewController: UIViewController {
    
    @IBOutlet weak var learningBacklogCollectionView: UICollectionView!
    @IBOutlet weak var seniorExplorersCollectionView: UICollectionView!
    @IBOutlet weak var juniorExplorersCollectionView: UICollectionView!
    var arrayOfLearningBacklog:[LearningBacklog] = []
    var arrayOfExplorers:[Explorers] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        learningBacklogCollectionView.delegate = self
        learningBacklogCollectionView.dataSource = self
        seniorExplorersCollectionView.delegate = self
        seniorExplorersCollectionView.dataSource = self
        juniorExplorersCollectionView.delegate = self
        juniorExplorersCollectionView.dataSource = self
        // Do any additional setup after loading the view.
        
       
        registerNib()
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        if(collectionView == seniorExplorersCollectionView){
            let explorersDetailsVC = storyboard.instantiateViewController(identifier: "ExplorersDetailViewController") as! ExplorersDetailViewController
            let senior = arrayOfExplorers.filter{(Explorers) -> Bool in
                               Explorers.status == .senior
                           }
            explorersDetailsVC.explorers = senior[indexPath.row]
            //
            //        challengeDetailsVC.challenges = arrayOfChallenges[indexPath.row]
                    navigationController?.pushViewController(explorersDetailsVC, animated: true  )
        }
        
        if(collectionView == juniorExplorersCollectionView){
            let explorersDetailsVC = storyboard.instantiateViewController(identifier: "ExplorersDetailViewController") as! ExplorersDetailViewController
            let junior = arrayOfExplorers.filter{(Explorers) -> Bool in
                Explorers.status == .junior
            }
                    explorersDetailsVC.explorers = junior[indexPath.row]
            
            //
            //        challengeDetailsVC.challenges = arrayOfChallenges[indexPath.row]
                    navigationController?.pushViewController(explorersDetailsVC, animated: true  )
        }

    }
    
    
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    
    func registerNib() {
        let nib = UINib(nibName: "LearningBacklogCollectionViewCell", bundle: nil)
        learningBacklogCollectionView?.register(nib, forCellWithReuseIdentifier: "LearningBacklogCollectionViewCellReuseIdentifier")
//        if let flowLayout = self.learningBacklogCollectionView?.collectionViewLayout as? UICollectionViewFlowLayout {
//            flowLayout.estimatedItemSize = CGSize(width: 1, height: 1)
//        }
        
        let nibSeniorExplorer = UINib(nibName: "SeniorCollectionViewCell", bundle: nil)
        seniorExplorersCollectionView?.register(nibSeniorExplorer, forCellWithReuseIdentifier: "SeniorCollectionViewCellReuseIdentifier")
        let nibJuniorExplorer = UINib(nibName: "JuniorCollectionViewCell", bundle: nil)
        juniorExplorersCollectionView.register(nibJuniorExplorer, forCellWithReuseIdentifier: "JuniorCollectionViewCellReuseIdentifier")
        
    }

}

extension ExplorersViewController: UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrayOfLearningBacklog.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if(collectionView == learningBacklogCollectionView){
            if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "LearningBacklogCollectionViewCellReuseIdentifier",
                                                             for: indexPath) as? LearningBacklogCollectionViewCell {
                let title = arrayOfLearningBacklog[indexPath.row].title
              let description = arrayOfLearningBacklog[indexPath.row].description
                let date = arrayOfLearningBacklog[indexPath.row].date
                
                cell.configureCell(title: title,description: description, date: date)
                cell.layer.cornerRadius = 10
                cell.layer.masksToBounds = true
                return cell
            }
        }
        
        if(collectionView == seniorExplorersCollectionView){
            if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SeniorCollectionViewCellReuseIdentifier",
                                                             for: indexPath) as? SeniorCollectionViewCell {
                let senior = arrayOfExplorers.filter{(Explorers) -> Bool in
                    Explorers.status == .senior
                }
                let name = senior[indexPath.row].name
              let role = senior[indexPath.row].role
                let image = senior[indexPath.row].photoAssetName
                
                cell.configureCell(name: name,role: role, image: image)
                cell.layer.cornerRadius = 10
                cell.layer.masksToBounds = true
                return cell
            }
        }
        
        if (collectionView == juniorExplorersCollectionView){
            if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "JuniorCollectionViewCellReuseIdentifier",
                                                             for: indexPath) as? JuniorCollectionViewCell {
                let junior = arrayOfExplorers.filter{(Explorers) -> Bool in
                    Explorers.status == .junior
                }
                let name = junior[indexPath.row].name
              let role = junior[indexPath.row].role
                let image = junior[indexPath.row].photoAssetName
                
                cell.configureCell(name: name,role: role, image: image)
                cell.layer.cornerRadius = 10
                cell.layer.masksToBounds = true
                return cell
            }
        }
        
        return UICollectionViewCell()
    }
    
    
}

extension ExplorersViewController: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
//        guard let cell: ChallengesCollectionViewCell = Bundle.main.loadNibNamed("ChallengesCollectionViewCell",
//                                                                             owner: self,
//                                                                             options: nil)?.first as? ChallengesCollectionViewCell else {
//                   return CGSize.zero
//               }
//        cell.configureCell(title: titles[indexPath.row],subtitle: subtitles[indexPath.row], image: images[indexPath.row])
//
//        cell.setNeedsLayout()
//        cell.layoutIfNeeded()
//        let size: CGSize = cell.contentView.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize)
        return CGSize(width: 320, height: 120 )
        
    }
    
   
    
}
