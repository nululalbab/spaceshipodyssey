import Foundation

enum ExplorerStatus {
    case junior
    case senior
}

struct Explorers {
   var name: String
    var status: ExplorerStatus
    var shift: String
    var role: String
    var photoAssetName: String
    var iWantToLearn: String
    var iCanHelpWith: String
}

