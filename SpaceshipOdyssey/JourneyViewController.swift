//
//  MainMenuViewController.swift
//  SpaceshipOdyssey
//
//  Created by Najibullah Ulul Albab on 12/04/20.
//  Copyright © 2020 Najibullah Ulul Albab. All rights reserved.
//

import UIKit
import CloudTagView

class JourneyViewController: UIViewController{

    
    
    @IBOutlet weak var challengesCollectionView: UICollectionView!
    
    @IBOutlet weak var activitiesCollectionView: UICollectionView!
    var arrayOfChallenges:[Challenge] = []
    var arrayOfActivities:[Activities] = []
    var challengeIndex:Int = 0
    let cloudView = CloudTagView()
    override func viewDidLoad() {
        super.viewDidLoad()
        challengesCollectionView.delegate = self
        challengesCollectionView.dataSource = self
        activitiesCollectionView.delegate = self
        activitiesCollectionView.dataSource = self
        cloudView.frame = CGRect(x: 0, y: 20, width: view.frame.width, height: 10)
        view.addSubview(cloudView)
        
        registerNib()
        // Do any additional setup after loading the view.
    }
    
    fileprivate func setupTags() {
        let tags = ["This", "is", "a", "example", "of", "Cloud", "Tag", "View"]

        for tag in tags {
            cloudView.tags.append(TagView(text: tag))
        }
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if(collectionView == challengesCollectionView){
            let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
            let challengeDetailsVC = storyboard.instantiateViewController(identifier: "ChallengesDetailsViewController") as! ChallengesDetailsViewController
            challengeDetailsVC.titleLabel = arrayOfChallenges[indexPath.row].title as String
            challengeDetailsVC.descriptionLabel = arrayOfChallenges[indexPath.row].description as String
            challengeDetailsVC.imageView = arrayOfChallenges[indexPath.row].image as String
            challengeDetailsVC.challenges = arrayOfChallenges[indexPath.row]
            navigationController?.pushViewController(challengeDetailsVC, animated: true  )
            
        }
        

    }
    
    
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    
    func registerNib() {
        let nib = UINib(nibName: "ChallengesCollectionViewCell", bundle: nil)
        challengesCollectionView?.register(nib, forCellWithReuseIdentifier: ChallengesCollectionViewCell.reuseIdentifier)
        if let flowLayout = self.challengesCollectionView?.collectionViewLayout as? UICollectionViewFlowLayout {
            flowLayout.estimatedItemSize = CGSize(width: 1, height: 1)
        }
        let nibActivities = UINib(nibName: "ActivitiesCollectionViewCell", bundle: nil)
        activitiesCollectionView?.register(nibActivities, forCellWithReuseIdentifier: "ActivitiesCollectionViewCellReuseIdentifier")
    }
    

}



extension JourneyViewController: UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if(collectionView == challengesCollectionView){
            let section=arrayOfChallenges.count
            
        }
        if(collectionView == activitiesCollectionView){
            let section = arrayOfActivities.count
        }
        return arrayOfChallenges.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if(collectionView == challengesCollectionView){
            if let cell = challengesCollectionView.dequeueReusableCell(withReuseIdentifier: ChallengesCollectionViewCell.reuseIdentifier,
                                                             for: indexPath) as? ChallengesCollectionViewCell {
                let title = arrayOfChallenges[indexPath.row].title
                let subtitle = arrayOfChallenges[indexPath.row].description
                let image = arrayOfChallenges[indexPath.row].image
                cell.configureCell(title: title,subtitle: subtitle, image: image)
                cell.layer.cornerRadius = 10
                cell.layer.masksToBounds = true
                return cell
            }
        }
        
        
        
        if(collectionView == activitiesCollectionView){
            if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ActivitiesCollectionViewCellReuseIdentifier",
                                                             for: indexPath) as? ActivitiesCollectionViewCell {
                
                let name = arrayOfActivities[indexPath.row].name
                let description = arrayOfActivities[indexPath.row].description
                let image = arrayOfActivities[indexPath.row].photoAssetName
                
                cell.configureCell(name: name, description: description, image: image)
                cell.layer.cornerRadius = 10
                cell.layer.masksToBounds = true
                return cell
            }
        }
        return UICollectionViewCell()
    }
    
    
}

extension JourneyViewController: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
//        guard let cell: ChallengesCollectionViewCell = Bundle.main.loadNibNamed("ChallengesCollectionViewCell",
//                                                                             owner: self,
//                                                                             options: nil)?.first as? ChallengesCollectionViewCell else {
//                   return CGSize.zero
//               }
//        cell.configureCell(title: titles[indexPath.row],subtitle: subtitles[indexPath.row], image: images[indexPath.row])
//
//        cell.setNeedsLayout()
//        cell.layoutIfNeeded()
//        let size: CGSize = cell.contentView.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize)
        return CGSize(width: 320, height: 320)
        
    }
    
   
    
}
