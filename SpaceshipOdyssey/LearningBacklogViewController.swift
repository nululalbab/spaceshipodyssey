//
//  LearningBacklogViewController.swift
//  SpaceshipOdyssey
//
//  Created by Najibullah Ulul Albab on 13/04/20.
//  Copyright © 2020 Najibullah Ulul Albab. All rights reserved.
//

import UIKit

class LearningBacklogViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    var learningBacklog = ["Implement Data Source","Implementing Core Data"]
    var learningBacklogDescription = "kjhsakdhasndkznxcknzxojfoasjdpasdjaslndklasdlksajldsaojdoauseoqwlenqwmenqwo"
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self
        tableView.delegate = self
        // Do any additional setup after loading the view.
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension LearningBacklogViewController: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return learningBacklog.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LearningBacklogTableViewCell", for: indexPath) as! LearningBacklogTableViewCell
        
        return cell
    }
    
}

extension LearningBacklogViewController: UITableViewDelegate{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
