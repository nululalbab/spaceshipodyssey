//
//  LearningBacklog.swift
//  SpaceshipOdyssey
//
//  Created by Najibullah Ulul Albab on 14/04/20.
//  Copyright © 2020 Najibullah Ulul Albab. All rights reserved.
//

import Foundation

struct LearningBacklog {
    var title:String
    var description:String
    var date:String
    var resources:[String]
}
