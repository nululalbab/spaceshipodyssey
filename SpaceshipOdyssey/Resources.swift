//
//  Resources.swift
//  SpaceshipOdyssey
//
//  Created by Najibullah Ulul Albab on 16/04/20.
//  Copyright © 2020 Najibullah Ulul Albab. All rights reserved.
//

import Foundation

struct Resources {
   var name: String
    var photoAssetName: String
    var description: String
}
