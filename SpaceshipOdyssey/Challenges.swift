//
//  Challenges.swift
//  SpaceshipOdyssey
//
//  Created by Najibullah Ulul Albab on 13/04/20.
//  Copyright © 2020 Najibullah Ulul Albab. All rights reserved.
//

import Foundation

struct Challenge {
    var title:String
    var description:String
    var duration:Int
    var goals:[String]
    var image:String
    var team:String
    var type:String
    var constraints: [String]
    var deliverables: [String]
}


