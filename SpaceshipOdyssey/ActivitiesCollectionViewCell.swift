//
//  ActivitiesCollectionViewCell.swift
//  SpaceshipOdyssey
//
//  Created by Najibullah Ulul Albab on 16/04/20.
//  Copyright © 2020 Najibullah Ulul Albab. All rights reserved.
//

import UIKit

class ActivitiesCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var imagePoster: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configureCell(name: String, description: String, image: String) {
        self.nameLabel.text = name
        self.descriptionLabel.text = description
        self.imagePoster.image = UIImage(named: image)
    }

}
